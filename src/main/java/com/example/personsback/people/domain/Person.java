package com.example.personsback.people.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.*;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "people")
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false)
    @NotBlank(message = "EL nombre es requerido")
    private String firstName;

    @Column(name = "last_name", nullable = false)
    @NotBlank(message = "EL apellido es requerido")
    private String lastName;

    @Column(name = "email", nullable = false)
    @NotBlank(message = "EL correo es requerido")
    private String email;

}
