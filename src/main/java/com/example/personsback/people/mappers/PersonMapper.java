package com.example.personsback.people.mappers;

import com.example.personsback.people.domain.Person;
import com.example.personsback.people.dtos.PersonDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface PersonMapper {

    PersonDTO toPersonDTO(Person person);

    @InheritInverseConfiguration
    Person toPerson(PersonDTO personDTO);
}
