package com.example.personsback.people.dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PersonDTO {

    private Long id;

    private String firstName;

    private String lastName;

    private String email;

}
