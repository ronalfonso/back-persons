package com.example.personsback.people.controller.resource;

import com.example.personsback.people.dtos.PersonDTO;
import org.springframework.hateoas.RepresentationModel;

public class PersonResource extends RepresentationModel<PersonResource> {

    private PersonDTO person;

    public PersonResource(PersonDTO person) {
        this.person = person;
    }

    // Getters y setters
    public PersonDTO getPerson() {
        return person;
    }

    public void setPerson(PersonDTO person) {
        this.person = person;
    }
}
