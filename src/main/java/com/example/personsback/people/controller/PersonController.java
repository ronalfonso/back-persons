package com.example.personsback.people.controller;


import com.example.personsback.people.controller.resource.PersonResource;
import com.example.personsback.people.dtos.PersonDTO;
import com.example.personsback.people.service.PersonService;
import exceptions.ResourceNotFoundException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.hateoas.EntityModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/api/persons")
@RequiredArgsConstructor
public class PersonController {

    private final PersonService personService;

    @Operation(summary = "Obtener todas las personas", description = "Recupera una lista paginada de personas")
    @ApiResponse(responseCode = "200", description = "Operación exitosa", content = @Content(mediaType = "application/json", schema = @Schema(implementation = PersonDTO.class)))
    @GetMapping
    public ResponseEntity<List<EntityModel<PersonDTO>>> getAllPersons(
            @Parameter(description = "Número de página para la paginación") @RequestParam(name = "page", defaultValue = "0") int page,
            @Parameter(description = "Tamaño de página para la paginación") @RequestParam(name = "size", defaultValue = "10") int size,
            @Parameter(description = "Filtro por nombre de persona") @RequestParam(name = "name", required = false) String name) {

        List<PersonDTO> personDTOList = personService.findAll(page, size, name);

        List<EntityModel<PersonDTO>> resources = personDTOList.stream()
                .map(personDTO -> EntityModel.of(personDTO,
                        linkTo(methodOn(PersonController.class).findById(personDTO.getId())).withSelfRel()))
                .collect(Collectors.toList());

        return new ResponseEntity<>(resources, HttpStatus.OK);
    }

    @Operation(summary = "Buscar persona por ID", description = "Busca una persona por su ID")
    @ApiResponse(responseCode = "200", description = "Persona encontrada", content = @Content(mediaType = "application/json", schema = @Schema(implementation = PersonDTO.class)))
    @ApiResponse(responseCode = "404", description = "Persona no encontrada")
    @GetMapping("/{id}")
    public ResponseEntity<PersonResource> findById(@PathVariable Long id) {
            PersonDTO personDTO = personService.findById(id);
            PersonResource personResource = new PersonResource(personDTO);
            personResource.add(linkTo(methodOn(PersonController.class).findById(personDTO.getId())).withSelfRel());
            return ResponseEntity.ok(personResource);
    }

    @Operation(summary = "Crear una nueva persona", description = "Crea una nueva persona y la guarda en la base de datos")
    @ApiResponse(responseCode = "201", description = "Persona creada exitosamente", content = @Content(mediaType = "application/json", schema = @Schema(implementation = PersonDTO.class)))
    @PostMapping
    public ResponseEntity<EntityModel<PersonDTO>> createPerson(@RequestBody PersonDTO personDTO) {
        PersonDTO newPerson = personService.create(personDTO);

        EntityModel<PersonDTO> resource = EntityModel.of(newPerson,
                linkTo(methodOn(PersonController.class).findById(newPerson.getId())).withSelfRel());
        return new ResponseEntity<>(resource, HttpStatus.CREATED);
    }

    @Operation(summary = "Actualizar una persona", description = "Actualiza la información de una persona existente")
    @ApiResponse(responseCode = "200", description = "Persona actualizada exitosamente", content = @Content(mediaType = "application/json", schema = @Schema(implementation = PersonDTO.class)))
    @ApiResponse(responseCode = "404", description = "Persona no encontrada")
    @PutMapping("/{id}")
    public ResponseEntity<EntityModel<PersonDTO>> updatePerson(@PathVariable Long id, @RequestBody PersonDTO personDTO) {
        PersonDTO updatedPerson = personService.update(id, personDTO);
        EntityModel<PersonDTO> resource = EntityModel.of(updatedPerson,
                linkTo(methodOn(PersonController.class).findById(updatedPerson.getId())).withSelfRel());

        return ResponseEntity.ok(resource);
    }

    @Operation(summary = "Eliminar una persona", description = "Elimina una persona de la base de datos")
    @ApiResponse(responseCode = "204", description = "Persona eliminada exitosamente")
    @ApiResponse(responseCode = "404", description = "Persona no encontrada")
    @DeleteMapping("/{id}")
    public ResponseEntity<Object> delete(@PathVariable Long id) {
        try {
            personService.delete(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

}


