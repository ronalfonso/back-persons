package com.example.personsback.people.service;

import com.example.personsback.people.dtos.PersonDTO;

import java.util.List;

public interface PersonService {

    PersonDTO findById(Long id);

    List<PersonDTO> findAll(int page, int size, String perNombre);

    PersonDTO create(PersonDTO personDTO);

    PersonDTO update(Long id, PersonDTO personDTO);

    void delete(Long id);
}
