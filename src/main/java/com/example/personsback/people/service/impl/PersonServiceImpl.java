package com.example.personsback.people.service.impl;

import com.example.personsback.people.domain.Person;
import com.example.personsback.people.dtos.PersonDTO;
import com.example.personsback.people.mappers.PersonMapper;
import com.example.personsback.people.repository.PersonRepository;
import com.example.personsback.people.service.PersonService;
import exceptions.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
@Validated
public class PersonServiceImpl implements PersonService {


    private final PersonRepository personRepository;

    private final PersonMapper personMapper;

    public List<PersonDTO> findAll(@RequestParam(name = "page", defaultValue = "0") int page,
                                   @RequestParam(name = "size", defaultValue = "10") int size,
                                   @RequestParam(name = "name", required = false) String name) {

        Pageable pageable = PageRequest.of(page, size);

        Specification<Person> specification = null;

        if (name != null) {
            if (specification != null) {
                specification = specification.and((persona, criteria, cb) -> cb.like(persona.get("firstName"), "%" + name + "%"));
            } else {
                specification = Specification.where((persona, criteria, cb) -> cb.like(persona.get("firstName"), "%" + name + "%"));
            }
        }
        Page<Person> personList = personRepository.findAll(specification, pageable);

        return personList.getContent().stream()
                .map(personMapper::toPersonDTO)
                .collect(Collectors.toList());
    }

    public PersonDTO findById(Long id) {
        Person person = getPerson(id);
        personNoExist(id, person);
        return personMapper.toPersonDTO(person);
    }

    @Override
    public PersonDTO create(PersonDTO personDTO) {
        return personMapper.toPersonDTO(save(personMapper.toPerson(personDTO)));
    }

    public PersonDTO update(Long id, PersonDTO personDTO) {
        Person person = getPerson(id);
        personNoExist(id, person);

        person.setFirstName(personDTO.getFirstName());
        person.setLastName(personDTO.getLastName());
        person.setEmail(personDTO.getEmail());

        return personMapper.toPersonDTO(save(person));
    }

    public void delete(Long id) {
        Person person = getPerson(id);
        personNoExist(id, person);
        personRepository.deleteById(id);
    }

    private Person getPerson(Long id) {
        return personRepository.findById(id).orElse(null);
    }

    private Person save(Person person) {
        return personRepository.save(person);
    }

    private static void personNoExist(Long id, Person person) {
        if (person == null) {
            throw new ResourceNotFoundException("No se encontro el recurso con id: " + id);
        }
    }
}
