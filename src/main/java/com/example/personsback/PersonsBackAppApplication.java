package com.example.personsback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories(basePackages = {"com.example.personsback.*"})
@EntityScan(basePackages = {"com.example.personsback.*"})
public class PersonsBackAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonsBackAppApplication.class, args);
    }
}
