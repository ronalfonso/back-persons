FROM openjdk:17-alpine as maven

WORKDIR /app

ARG JAR_FILE=target/persons-back-dev.jar

COPY ${JAR_FILE} /app/persons-back.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "persons-back.jar"]
